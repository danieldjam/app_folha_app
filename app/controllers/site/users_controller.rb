class Site::UsersController < ApplicationController
  http_basic_authenticate_with name: "@Pintos", password: "@PINTOS2020@@", only: [:index]
  before_action :verify_password, only:[:update]
  before_action :set_user , only: [:show, :edit, :update, :destroy]
  
  def index 
    @users = User.all.page(params[:page]).per(25).order(:acesso)
    #@q = User.ransack(params[:q])
    #@users = @q.result(distinct: true)
  end

  def show
  end

  def edit
  end

  def new
   @user = User.new
  end

  def create
    @user = User.new(params_user)
    if @user.save()
      redirect_to site_users_index_path, notice: "Usuário Criado com Sucesso !"
    else
      render :new
    end
   
  end


  def update

    if @user.update(params_user)
    redirect_to site_welcome_index_path, notice: "Usuário Atualizado com Sucesso !"
    else
    render :edit
    end
 
  end


  def search
    
    #@q = User.ransack(params[:q])
    #@users = @q.result(distinct: true)

    #@users = User.all
    #@users = User.includes(:users)
    #.where("lower(acesso,chapa) LIKE ?",
    #"%#{params[:term].downcase}%")
    #.page(params[:page])
    

  end

 private

 def verify_password 
   if params[:user][:password].blank? && params[:user].blank?
    params[:user].extract!(:password)
   end 
 end

 def set_user
  @user = User.find(params[:id])
 end
 
 def params_user
   params.require(:user).permit(:chapa,:password,:acesso)
 end

end
