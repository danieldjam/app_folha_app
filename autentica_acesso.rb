require 'savon'

user = gets.chomp.to_s
pass = gets.chomp

@client = Savon.client(
  :wsdl => 'http://187.125.24.178:8052/wsDataServer/MEX?wsdl',
  :endpoint => 'http://187.125.24.178:8052/wsDataServer/IwsBase',
  :namespace => 'http://www.totvs.com/',
  basic_auth:[user,pass],
  element_form_default: :qualified,
  :log => true,
  :pretty_print_xml => true,
  ssl_verify_mode: :none,
  log_level: :debug, 
  :raise_errors => true,
)

puts @client.operations

message = {
  "Usuario" => user.to_s,
  "Senha" => pass,
}  

response = @client.call(
  :autentica_acesso,
  message: message,
  headers:{
    "Authorization" => 'Basic NDQ1NjowNDAxMTM=', 
    "Connection" => 'Keep-Alive', 
    "Accept-Encoding" => "gzip,deflate", 
    "Content-Type" => "text/xml;charset=utf-8",
  }
)

result = response.body[:autentica_acesso_response][:autentica_acesso_result]