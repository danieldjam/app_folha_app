const { environment } = require('@rails/webpacker')

const webpack = require('webpack')
environment.plugins.append('Provide', new webpack.ProvidePlugin({
  $: 'jquery/src/jquery.js',
  jQuery: 'jquery/src/jquery.js',
  Popper: ['popper.js', 'default']
}))

module.exports = environment
