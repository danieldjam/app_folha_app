Rails.application.routes.draw do
  namespace :login do
    get 'dasboard/index'
  end
  namespace :site do
    get 'users/index'
    resources :users
    get 'welcome/index'
    post 'welcome/index'
    #get 'welcome/destroy', only: [:destroy]
    delete 'welcome/destroy'
    get 'search', to: 'search#users'
  end

  namespace :login do
    get 'welcome/index'
    post 'welcome/index'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  root to: 'login/welcome#index' 
  
  #root to: 'login/dasboard#index'
  
  get '/inicio' , to: 'login/welcome#index'
  get '/contra-cheque', to: 'site/welcome#index'

end
